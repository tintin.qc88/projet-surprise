const {randomColor} = require('./Code.js');
const rgbHex = require ('rgb-hex');

describe('randomColor', () => {
    it('Returns color', () => {
        expect(rgbHex(randomColor())).toEqual("ffffff")
    })
})